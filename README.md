## Introduction
USB Digital Interface (Isolated), V1  
This software is used together with the Seeeduino XIAO development board and
an isolated circuit that allows reading the status of 2 inputs (IN1 and IN2) and drive 2 outputs (OUT1 and OUT2). (TTL 5V)  
The status of the 2 Inputs and 2 Outputs can be checked with the corresponding LED visible in the device.  
To Control the Digital Interface a USB-Serial communication (9600 Baud) is used.  
Linux users usually can use the device "/dev/ttyACM0" or identify the manufacturer "Seeed" to access this device.  
Python users can use the pyserial module:  
https://pyserial.readthedocs.io/en/latest/tools.html  
https://pyserial.readthedocs.io/en/latest/shortintro.html  
  
https://gitlab.ethz.ch/tiagof/USB_Digital_Interface  
Tiago Neves,  
ETH Zürich, LPC, 2022
### Foto
![USB_1](Images/USB_1.jpg)

### Quick Start
- Plug the USB Digital Interface to computer
- No diver is usually needed (Windows, Linux)
- Open a Serial interface program (Putty, Picocom, etc..) (9600 Baud) Ex.`picocom /dev/ttyACM0`
- type: help

### Usage
```
### Usage to r/w of IN1,IN2,OUT1 and OUT2 ###

OUT1=ON -> Switch ON OUTPUT1  
OUT2=ON -> Switch ON OUTPUT2  
OUT1=OFF -> Switch OFF OUTPUT1  
OUT2=OFF -> Switch OFF OUTPUT2  
IN1=? -> Reads the status of INPUT1, reply: IN1=ON  
IN2=? -> Reads the status of INPUT2, reply: IN2=OFF  
status -> Show the status of the Inputs and Outputs  
help -> Show this help  
version -> Show software version  
```
### STATUS LED
The Status LED has 4 Modes.  
- MODE 1: Startup blink every 0.3s -> waiting for serial communication  
- MODE 2: LED ON -> Device Connected and wiating for commands
- MODE 3: Blink Slow (2 times) -> New command is OK
- MODE 4: Blink Fast (3 times) -> New command is Failed

### ISSUES


1. Linux User cannot access Serial Port: `Permission denied:'/dev/ttyACM0'`   
Solution: `$sudo usermod -a -G dialout $USER`

## For Developers
Tools to make your own 
### Installing The libraries in Arduino 1.8.8
Open the Arduino Software.  
Edit Preferences: "File"->"Preferences"->  
Add the Seeeduino resources in the "Aditional Boards Manager URL's:"  
https://files.seeedstudio.com/arduino/package_seeeduino_boards_index.json  
Change the Project location in the arduino application in "Sketchbook location:"  
Should look like: /home/user/USB_Digital_Interface/  
![Preferences](Images/Preferences.png)

Install the SAMD BOARDS in: "Tools"->"Board->Board Manager"  
Search for "seeed" and install "Seeed SAMD Boards"  
![SAMD Boards](Images/SAMD_Boards.png)

Select the Seeeduino XIAO  
![Select Seeduino](Images/SelectSeeeduino.png)

### Hardware
![USB_2](Images/USB_2.jpg)
![Schematic](Images/Schematic.png)
