/* 
  USB Digital Interface (Isolated)V1
  Tiago Neves
  LPC, ETH Zürich
  2022
*/
#include <TimerTCC0.h>

#define STATUS_LED 0 //A0 or 0 is STATUS LED
#define OUT1 A9
#define OUT2 A10
#define IN1 A1
#define IN2 A2
#define LED_IN1 A5
#define LED_IN2 A6

//char *InputStatus[]={"","","",""}; //variable that stores the status of the 4 inputs (ON/OFF)
char *InputStatus[]={"",""}; //variable that stores the status of the 2 inputs (ON/OFF)
char *OutputStatus[]={"OFF","OFF"}; //variable that stores the status of the 2 outputs (ON/OFF)
String inputString = "";         // a String to hold incoming data
bool command_is_read = false;
bool serial_port_connected = false;
bool isLEDOn = false;
int blink_count=0;

void setup() 
{
  pinMode(STATUS_LED, OUTPUT);
  pinMode(IN1, INPUT);
  pinMode(IN2, INPUT);
  //pinMode(3, INPUT);
  //pinMode(4, INPUT);
  pinMode(LED_IN1, OUTPUT);
  pinMode(LED_IN2, OUTPUT);
  //pinMode(7, OUTPUT);
  //pinMode(8, OUTPUT);
  pinMode(OUT1, OUTPUT);  //A9 OUT1
  pinMode(OUT2, OUTPUT);  //A10 OUT2
  
  SerialUSB.begin(9600);
  
  inputString.reserve(200); // reserve 200 bytes for the inputString
  TimerTcc0.initialize(300000); //Initialize timer (0.3s) 
  TimerTcc0.attachInterrupt(timerIsr); //Enable timer
}

/* ## Startup Screen for Serial Port  ## */
void print_version (void)
{
  SerialUSB.println(F("USB_Digital_Interface_V1"));
}

/* ## Startup Screen for Serial Port  ## */
void print_startup (void)
{
  SerialUSB.println(F("Isolated Digital Reader V1, ETH Zürich, LPC"));
  SerialUSB.println(F("This Hardware contains 2 Digital Inputs and 2 Digital Outputs"));
  SerialUSB.println(F("Tiago Neves -2022-"));
  print_version();
}
/* ## Show help to for Serial Port  ## */
void print_help()
{
  SerialUSB.println(F("### Usage to r/w of IN1,IN2,OUT1 and OUT2 ###"));
  SerialUSB.println(F("OUT1=ON -> Switch ON OUTPUT1"));
  SerialUSB.println(F("OUT2=ON -> Switch ON OUTPUT2"));
  SerialUSB.println(F("OUT1=OFF -> Switch OFF OUTPUT1"));
  SerialUSB.println(F("OUT2=OFF -> Switch OFF OUTPUT2"));
  SerialUSB.println(F("IN1=? -> Reads the status of INPUT1, reply: IN1=ON"));
  SerialUSB.println(F("IN2=? -> Reads the status of INPUT2, reply: IN2=OFF"));
  SerialUSB.println(F("status ->Show the status of the Inputs and Outputs"));
  SerialUSB.println(F("help ->Show this help"));
  SerialUSB.println(F("version ->Show software version"));
}

/* ## Timer Interrupt Service Routine  ## */
void timerIsr()
{ 
  digitalWrite(STATUS_LED, isLEDOn);
  isLEDOn = !isLEDOn;
  if ( blink_count >= 1 ) blink_count--;
}

/* ## Main Loop ## */
void loop() 
{
  int i = 0;
  if (!serial_port_connected) {         //if tehre is no active serial port connection
    if (SerialUSB) {                    //Check if serial port is connected. Needed for native USB port only
      SerialUSB.println(F("Serial Starting 9600 Baud"));
      print_startup();
      TimerTcc0.detachInterrupt(); //Stop timer for blinking STATUS_LED
      digitalWrite(STATUS_LED, HIGH); //STATUS_LED is ON
      serial_port_connected = true;
    }
  }
  else{
    if ( blink_count <= 0 ){
      //SerialUSB.println(blink_count);
      TimerTcc0.detachInterrupt(); //Stop timer for blinking STATUS_LED
      digitalWrite(STATUS_LED, HIGH); //STATUS_LED is ON
    }
  }
  if  (digitalRead(IN1)) digitalWrite(LED_IN1, HIGH), InputStatus[0]="ON" ;
  else (digitalWrite(LED_IN1, LOW), InputStatus[0]="OFF" ) ;

  if  (digitalRead(IN2)) digitalWrite(LED_IN2, HIGH), InputStatus[1]="ON" ;
  else (digitalWrite(LED_IN2, LOW), InputStatus[1]="OFF" ) ;
  
  /*//reads the A1-A4 inputs, turns ON or OFF the Outputs A5-A8 and saves the status in InputStatus
  for ( i = 1; i < 5 ; i ++ ) {
    if  (digitalRead(i)) digitalWrite(i+4, HIGH), InputStatus[i-1]="ON" ;
    else (digitalWrite(i+4, LOW), InputStatus[i-1]="OFF" ) ;
  }*/

  if (Is_New_Command()) {
    Process_Command();    
    command_is_read = true;
  }
  //delay(1); // Delay 1ms
}

/* ## The new command is processed here ## */
void Process_Command (void)
{
  SerialUSB.println("");
  SerialUSB.print(F("New_command:"));
  SerialUSB.println(inputString); //print the command
  if( inputString.equalsIgnoreCase("version") ){
    SerialUSB.println(F("command=OK"));
    print_version();
    blink_STATUS(1);
  }
  else if( inputString.equalsIgnoreCase("help") ){
    SerialUSB.println(F("command=OK"));
    print_help();
    blink_STATUS(1);
  }
  else if( inputString.equalsIgnoreCase("status") ){
    SerialUSB.println(F("command=OK"));
    SerialUSB.println("IN1=" + String(InputStatus[0]));
    SerialUSB.println("IN2=" + String(InputStatus[1]));
    SerialUSB.println("OUT1=" + String(OutputStatus[0]));
    SerialUSB.println("OUT2=" + String(OutputStatus[1])); 
    blink_STATUS(1);
  }
  else if( inputString.equalsIgnoreCase("OUT1=ON") ){
    SerialUSB.println(F("command=OK"));
    digitalWrite(OUT1, HIGH);
    OutputStatus[0]="ON";
    blink_STATUS(1);
  }
  else if( inputString.equalsIgnoreCase("OUT1=OFF") ){
    SerialUSB.println(F("command=OK"));
    digitalWrite(OUT1, LOW);
    OutputStatus[0]="OFF";
    blink_STATUS(1);
  }
  else if( inputString.equalsIgnoreCase("OUT2=ON") ){
    SerialUSB.println(F("command=OK"));
    OutputStatus[1]="ON";
    digitalWrite(OUT2, HIGH);
    blink_STATUS(1);
  }
  else if( inputString.equalsIgnoreCase("OUT2=OFF") ){
    SerialUSB.println(F("command=OK"));
    OutputStatus[1]="OFF";
    digitalWrite(OUT2, LOW);
    blink_STATUS(1);
  }
  else if( inputString.equalsIgnoreCase("IN1=?") ){
    SerialUSB.println(F("command=OK"));
    SerialUSB.println("IN1=" + String(InputStatus[0]));
    blink_STATUS(1);
  }
  else if( inputString.equalsIgnoreCase("IN2=?") ){
    SerialUSB.println(F("command=OK"));
    SerialUSB.println("IN2=" + String(InputStatus[1]));
    blink_STATUS(1);
  }
  else {
      SerialUSB.println(F("command=FAIL >> Type: help"));
      blink_STATUS(0);
    }
}

/* ## Blinks the LED fast if FAIL and Slow if OK ## */
void blink_STATUS (bool status)
{
  if( status ){
    blink_count = 6;
    TimerTcc0.initialize(400000); //Initialize timer (0.4s) Blink Slow
    TimerTcc0.attachInterrupt(timerIsr); //Enable timer
    }
    else{
      blink_count = 10;
      TimerTcc0.initialize(100000); //Initialize timer (0.1s) Blink Fast
      TimerTcc0.attachInterrupt(timerIsr); //Enable timer
    }
}

/* ## Verify if there is a new Serial command ## */
bool Is_New_Command (void)
{
  if (command_is_read){
    inputString = "";
    command_is_read = false;
    }
  while (SerialUSB.available()) {
    char inChar = (char)SerialUSB.read();
    SerialUSB.print(inChar);               //Print the new character
    if (inChar == '\n' || inChar == '\r') { //if the incoming character is a newline (new command)
      return true;
    }
    else{
      inputString += inChar;  //Joins the new character to form a command
      return false;
    }
  }
}
